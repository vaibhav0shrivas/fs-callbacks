const problem2 = require("../problem2.cjs");

problem2();

problem2((error) => {
  if (error) {
    console.error(`Call to problem1((error) => {}) failed!`, error);
  } else {
    console.log("Succesfull exection of problem1((error) => {})");
  }
});

problem2(" ", " ", (error) => {
  if (error) {
    console.error(`Call to problem1(" ", " ", (error) => {}) failed!`, error);
  } else {
    console.log('Succesfull exection of problem1(" ", " ", (error) => {})');
  }
});

problem2("", "", (error) => {
  if (error) {
    console.error(`Call to problem1("", "", (error) => {}) failed!`, error);
  } else {
    console.log('Succesfull exection of problem1("", "", (error) => {})');
  }
});

problem2(1, " ", (error) => {
  if (error) {
    console.error(`Call to problem1(1, " ", (error) => {}) failed!`, error);
  } else {
    console.log('Succesfull exection of problem1(1, " ", (error) => {})');
  }
});

problem2("doesnotexit.txt", " ", (error) => {
  if (error) {
    console.error(
      `Call to problem1("doesnotexit.txt", " ", (error) => {}) failed!`,
      error
    );
  } else {
    console.log(
      'Succesfull exection of problem1("doesnotexit.txt", " ", (error) => {})'
    );
  }
});

problem2("lipsum.txt", "filenames.txt", (error) => {
  if (error) {
    console.error(
      `Call to problem2("lipsum.txt", "filenames.txt", (error) => {}) failed!`,
      error
    );
  } else {
    console.log(
      'Succesfull execution of problem2("lipsum.txt", "filenames.txt", (error) => {})'
    );
    problem2("filenames.txt", "filename2.txt", (error) => {
      if (error) {
        console.error(
          `Call to problem2("filenames.txt", "filename2.txt", (error) => {}) failed!`,
          error
        );
      } else {
        console.log(
          'Succesfull execution of problem2("filenames.txt", "filename2.txt", (error) => {})'
        );
      }
    });
  }
});
