/*
#### Problem 2

Using callbacks and the fs module's asynchronous functions, do the following:

1. Read the given file lipsum.txt
2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
3. Read the new file and convert it to lowercase. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
4. Read the new files, sort the content, and write it out to a new file. Store the name of the new file in filenames.txt
5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");
const path = require("path");

const deleteTheseFiles = function (filesPathList, callback) {
  let noOfFilesRemaining = filesPathList.length;
  filesPathList.forEach((filepath) => {
    fs.unlink(filepath, (error) => {
      noOfFilesRemaining--;
      if (error) {
        callback(error);
        return;
      } else if (noOfFilesRemaining <= 0) {
        callback(null);
      }
    });
  });
};

const problem2 = function (fileToBeRead, fileToStoreFileNames, callback) {
  if (
    fileToBeRead === undefined ||
    fileToStoreFileNames === undefined ||
    callback === undefined ||
    typeof fileToBeRead !== "string" ||
    typeof fileToStoreFileNames !== "string" ||
    typeof callback !== "function" ||
    fileToBeRead === "" ||
    fileToStoreFileNames === "" ||
    fileToStoreFileNames === " " ||
    fileToBeRead === " "
  ) {
    console.log("Please Provide valid arguments");
    if (typeof callback === "function") {
      callback(new Error("Invalid arguments to problem2()"));
    }
    return;
  }
  fs.readFile(
    path.join(__dirname, fileToBeRead),
    "utf-8",
    (error, dataFromtxtFile) => {
      if (error) {
        console.error(`Failed to read ${fileToBeRead}.`);
        callback(error);
      } else {
        console.log(`Read ${fileToBeRead} sucessfully.`);
        fs.writeFile(
          path.join(__dirname, "UPPERCASE.txt"),
          dataFromtxtFile.toUpperCase(),
          { encoding: "utf-8", flag: "w" },
          (error) => {
            if (error) {
              console.error("Failed to create and write to UPPERCASE.txt");
              callback(error);
            } else {
              console.log("Wrote to UPPERCASE.txt sucessfully.");
              //storing filename in another text file
              fs.writeFile(
                path.join(__dirname, fileToStoreFileNames),
                "UPPERCASE.txt" + "\n",
                { encoding: "utf-8", flag: "a" },
                (error) => {
                  if (error) {
                    console.error(
                      `Failed to write file name into ${fileToStoreFileNames}.`
                    );
                    callback(error);
                  } else {
                    console.log(
                      `Wrote filename "UPPERCASE.txt" into ${fileToStoreFileNames}.`
                    );
                    fs.readFile(
                      path.join(__dirname, "UPPERCASE.txt"),
                      "utf-8",
                      (error, dataFromUppercaseFile) => {
                        if (error) {
                          console.error(`Failed to read "UPPERCASE.txt".`);
                        } else {
                          console.log(`Read "UPPERCASE.txt" sucessfully.`);
                          fs.writeFile(
                            path.join(__dirname, "lowercaseSentences.txt"),
                            JSON.stringify(
                              dataFromUppercaseFile
                                .toLowerCase()
                                .split("\n")
                                .filter((sentence) => {
                                  return sentence !== "";
                                })
                            ),
                            "utf-8",
                            (error) => {
                              if (error) {
                                console.error(
                                  `Failed to write into lowercaseSentences.txt.`
                                );
                                callback(error);
                              } else {
                                console.log(
                                  `Wrote lowercase sentences into lowercaseSentences.txt`
                                );
                                fs.writeFile(
                                  path.join(__dirname, fileToStoreFileNames),
                                  "lowercaseSentences.txt" + "\n",
                                  { encoding: "utf-8", flag: "a" },
                                  (error) => {
                                    if (error) {
                                      console.error(
                                        `Failed to write file name into ${fileToStoreFileNames}.`
                                      );
                                      callback(error);
                                    } else {
                                      console.log(
                                        `Wrote filename "lowercaseSentences.txt" into ${fileToStoreFileNames}.`
                                      );

                                      fs.readFile(
                                        path.join(
                                          __dirname,
                                          "lowercaseSentences.txt"
                                        ),
                                        "utf-8",
                                        (error, dataFromLowercaseFile) => {
                                          if (error) {
                                            console.error(
                                              `Failed to read "lowercaseSentences.txt".`
                                            );
                                          } else {
                                            console.log(
                                              `Read "lowercaseSentences.txt" sucessfully.`
                                            );
                                            fs.writeFile(
                                              path.join(
                                                __dirname,
                                                "sortedSentences.txt"
                                              ),
                                              JSON.stringify(
                                                JSON.parse(
                                                  dataFromLowercaseFile
                                                ).sort()
                                              ),
                                              "utf-8",
                                              (error) => {
                                                if (error) {
                                                  console.error(
                                                    `Failed to write into "sortedSentences.txt".`
                                                  );
                                                  callback(error);
                                                } else {
                                                  console.log(
                                                    `Wrote sorted lowercase sentences into "sortedSentences.txt".`
                                                  );
                                                  fs.writeFile(
                                                    path.join(
                                                      __dirname,
                                                      fileToStoreFileNames
                                                    ),
                                                    "sortedSentences.txt" +
                                                      "\n",
                                                    {
                                                      encoding: "utf-8",
                                                      flag: "a",
                                                    },
                                                    (error) => {
                                                      if (error) {
                                                        console.error(
                                                          `Failed to write file name into ${fileToStoreFileNames}.`
                                                        );
                                                        callback(error);
                                                      } else {
                                                        console.log(
                                                          `Wrote filename "sortedSentences.txt" into ${fileToStoreFileNames}.`
                                                        );
                                                        fs.readFile(
                                                          path.join(
                                                            __dirname,
                                                            fileToStoreFileNames
                                                          ),
                                                          "utf-8",
                                                          (
                                                            error,
                                                            dataFromListOfFiles
                                                          ) => {
                                                            if (error) {
                                                              console.error(
                                                                `Failed to read file names from ${fileToStoreFileNames}!`
                                                              );
                                                              callback(error);
                                                            } else {
                                                              console.log(
                                                                `Read file names from ${fileToStoreFileNames}`
                                                              );

                                                              let listOfFilePathToBeDeleted =
                                                                dataFromListOfFiles
                                                                  .trim()
                                                                  .split("\n")
                                                                  .map(
                                                                    (
                                                                      filename
                                                                    ) => {
                                                                      return path.join(
                                                                        __dirname,
                                                                        filename
                                                                      );
                                                                    }
                                                                  );

                                                              deleteTheseFiles(
                                                                listOfFilePathToBeDeleted,
                                                                (error) => {
                                                                  if (error) {
                                                                    console.error(
                                                                      `Failed call to deleteTheseFiles()!`
                                                                    );
                                                                    callback(
                                                                      error
                                                                    );
                                                                  } else {
                                                                    console.log(
                                                                      "Deleted all new files."
                                                                    );
                                                                    callback(
                                                                      null
                                                                    );
                                                                  }
                                                                }
                                                              );
                                                            }
                                                          }
                                                        );
                                                      }
                                                    }
                                                  );
                                                }
                                              }
                                            );
                                          }
                                        }
                                      );
                                    }
                                  }
                                );
                              }
                            }
                          );
                        }
                      }
                    );
                  }
                }
              );
            }
          }
        );
      }
    }
  );
};

module.exports = problem2;
