const problem1 = require("../problem1.cjs");

console.log("test problem1()------------------------------------------");
problem1();

console.log(
  "test problem1('blah',2)------------------------------------------"
);
problem1("blah", 2);

console.log("test problem1('temp')------------------------------------------");
problem1("temp");

console.log("test problem1(2)------------------------------------------");
problem1(2);

console.log(
  "test problem1('temp folder',3,(error)=>{})------------------------------------------"
);

problem1("temp folder", 3, (error) => {
  if (error) {
    console.error("Problem1 encountered error", error);
  } else {
    console.log(
      "test problem1('temp folder',3,(error)=>{}) sucessfully executed."
    );
    console.log(
      "test problem1('tempder',2.1,(error)=>{})------------------------------------------"
    );
    problem1("tempder", 2.1, (error) => {
      if (error) {
        console.error("Problem1 encountered error", error);
      } else {
        console.log(
          "test problem1('tempder',2.1,(error)=>{}) sucessfully executed."
        );
      }
    });
  }
});
