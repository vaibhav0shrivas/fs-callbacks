/*
Problem 1
Using callbacks and the fs module's asynchronous functions, do the following:
    1. Create a directory of random JSON files.
    2. Delete those files simultaneously.
*/

const crypto = require("crypto");
const fs = require("fs");
const path = require("path");

const getRandomObject = function (bufferObject, callback) {
  crypto.randomFill(bufferObject, 5, (error, bufferObject) => {
    if (error) {
      console.error("Failed to create random buffer object.");
      callback(error);
    } else {
      console.log("Random buffer object created sucessfully.");
      callback(null, bufferObject);
    }
  });
};

const createNJSONFiles = function (numberOfFIles, directoryPath, callback) {
  let bufferObject = Buffer.alloc(10);
  let filesPathsList = [];
  let noOfFilesLeftToBeCreated = numberOfFIles;
  for (let index = 0; index < numberOfFIles; index++) {
    getRandomObject(bufferObject, (error, bufferObject) => {
      if (error) {
        console.error("call to getRandomObject failed.");
        callback(error);
      } else {
        fs.writeFile(
          path.join(directoryPath, `${index}.json`),
          bufferObject.toString(),
          (error) => {
            noOfFilesLeftToBeCreated--;
            filesPathsList.push(path.join(directoryPath, `${index}.json`));
            if (error) {
              console.error(`Failed to write to ${index}.json.`);
              callback(error);
            } else if (noOfFilesLeftToBeCreated <= 0) {
              callback(null, filesPathsList);
            }
          }
        );
      }
    });
  }
};

const deleteTheseFiles = function (filesPathList, callback) {
  let noOfFilesRemaining = filesPathList.length;
  filesPathList.forEach((filepath) => {
    fs.unlink(filepath, (error) => {
      noOfFilesRemaining--;
      if (error) {
        console.error(`Failed to delete ${filepath}.`);
        callback(error);
        return;
      } else if (noOfFilesRemaining <= 0) {
        callback(null);
      }
    });
  });
};

/**
 *
 * @param {string} folderName Folder name in which files will be created.
 * @param {number} numberOfFIles Number of files to be created.
 * @param {function} callback Callback function of the form (error) =>{}.
 */
const problem1 = function (folderName, numberOfFIles, callback) {
  if (
    folderName === undefined ||
    numberOfFIles === undefined ||
    callback === undefined ||
    Number.isInteger(numberOfFIles) == false ||
    typeof callback !== "function"
  ) {
    console.log("Please Provide valid arguments");
    return;
  }

  const directoryForJSONFilesPath = path.join(__dirname, `${folderName}/`);
  fs.mkdir(directoryForJSONFilesPath, { recursive: true }, (error) => {
    if (error) {
      console.error("Directory creation failed!");
      callback(error);
    } else {
      console.log("Directory created sucessfully.");
      createNJSONFiles(
        numberOfFIles,
        directoryForJSONFilesPath,
        (error, filesPathList) => {
          if (error) {
            console.error("Failed call to createNJSONFiles()!");
            callback(error);
          } else {
            console.log("Files cration sucessfull!");

            console.log("File Created are", filesPathList);

            deleteTheseFiles(filesPathList, (error) => {
              if (error) {
                console.error("Failed call to deleteTheseFiles()!");
                callback(error);
              } else {
                console.log("All JSON files deleted");
                callback(null);
              }
            });
          }
        }
      );
    }
  });
};

module.exports = problem1;
